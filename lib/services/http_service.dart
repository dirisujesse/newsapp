import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';

class HttpService {
  final http = Dio(
    BaseOptions(
      baseUrl: "http://newsapi.org/v2",
      connectTimeout: 1000 * 5,
      receiveTimeout: 1000 * 5,
    ),
  )..interceptors.add(InterceptorsWrapper(
    onRequest: (RequestOptions opts) {
      opts.queryParameters["apiKey"] = "8ede408967b44f1399e69ee5f1a05429";
      opts.queryParameters["pageSize"] = 30;
      return opts;
    }
  ));

  Future<Map<dynamic, dynamic>> worldNews() async {
    try {
      final req = await http.get("/top-headlines?language=en");
      return req.data;
    } on DioError catch(e) {
      throw e.response.data;
    }
  }

  Future<Map<dynamic, dynamic>> getHeadlines({@required by, @required value}) async {
    try {
      final req = await http.get("/top-headlines?$by=$value");
      return req.data;
    } on DioError catch(e) {
      throw e.response.data;
    }
  }


  Future<Map<dynamic, dynamic>> getSources() async {
    try {
      final req = await http.get("/sources?language=en");
      return req.data;
    } on DioError catch(e) {
      print(e.response.data);
      throw e.response.data;
    }
  }

}