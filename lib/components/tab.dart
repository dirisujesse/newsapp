import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:newsapp/models/news_model.dart';
import 'package:newsapp/services/http_service.dart';
import 'package:newsapp/styles/colors.dart';

final http = HttpService();
final List<Future<Map<dynamic, dynamic>>> tabs = [
  http.worldNews(),
  http.getHeadlines(by: "country", value: "ng"),
  http.getHeadlines(by: "country", value: "us"),
  http.getHeadlines(by: "country", value: "gb"),
  http.getHeadlines(by: "country", value: "za"),
];

class HomeComponent extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _HomeComponentState();
  }
}

class _HomeComponentState extends State<HomeComponent>
    with SingleTickerProviderStateMixin {
  TabController _ctrl;

  @override
  void initState() {
    _ctrl = TabController(vsync: this, length: 5);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return NestedScrollView(
      headerSliverBuilder: (context, _) {
        return [
          SliverAppBar(
            pinned: true,
            backgroundColor: appBlack,
            title: Text("News App"),
            centerTitle: true,
            bottom: TabBar(
              controller: _ctrl,
              isScrollable: true,
              tabs: [
                Tab(
                  text: "Home",
                ),
                Tab(
                  text: "Nigeria",
                ),
                Tab(
                  text: "USA",
                ),
                Tab(
                  text: "UK",
                ),
                Tab(
                  text: "South Africa",
                ),
              ],
            ),
          )
        ];
      },
      body: TabBarView(
        children: List.generate(tabs.length, (idx) {
          return FutureBuilder(
            future: tabs[idx],
            builder: (context, AsyncSnapshot<Map<dynamic, dynamic>> fut) {
              if (fut.connectionState == ConnectionState.waiting) {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
              if (fut.hasError) {
                return Center(
                  child: Text("Ooops data could not be fetched at this time", textAlign: TextAlign.center,),
                );
              }
              final data = fut.data["articles"];
              return ListView.builder(
                padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 20.0),
                itemCount: data.length,
                itemBuilder: (context, idx) {
                  final Articles article =
                      Articles.fromJson(Map<String, dynamic>.from(data[idx]));
                  return GestureDetector(
                    child: Card(
                      elevation: 0,
                      shape: RoundedRectangleBorder(
                        side: BorderSide(color: appBlack, width: 1),
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      margin: EdgeInsets.only(bottom: 20.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.symmetric(
                              horizontal: 14.0,
                              vertical: 14.0,
                            ),
                            decoration: BoxDecoration(
                              border: Border(
                                bottom: BorderSide(color: appBlack, width: 1),
                              ),
                            ),
                            child: Text(
                              article?.title ?? "",
                              style: Theme.of(context).textTheme.body2.copyWith(
                                  fontSize: 16, fontWeight: FontWeight.w600),
                              textAlign: TextAlign.start,
                              maxLines: 1,
                            ),
                          ),
                          AspectRatio(
                            aspectRatio: 5 / 2,
                            child: article.urlToImage != null
                                ? Image.network(
                                    article?.urlToImage ?? "",
                                    fit: BoxFit.cover,
                                  )
                                : Padding(
                                    padding: EdgeInsets.all(20),
                                    child: Center(
                                      child: Text(
                                        article?.description ??
                                            article?.content ??
                                            article?.title ??
                                            "",
                                        maxLines: 3,
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                  ),
                          ),
                          Container(
                            decoration: BoxDecoration(
                              border: Border(
                                top: BorderSide(color: appBlack, width: 1),
                              ),
                            ),
                            child: Table(
                              defaultVerticalAlignment:
                                  TableCellVerticalAlignment.middle,
                              children: <TableRow>[
                                TableRow(
                                  children: [
                                    Padding(
                                      padding: EdgeInsets.only(left: 10),
                                      child: Text(
                                        article?.author ?? "News App",
                                        style:
                                            Theme.of(context).textTheme.body2,
                                        maxLines: 1,
                                      ),
                                    ),
                                    Align(
                                      alignment: Alignment.centerRight,
                                      child: FlatButton.icon(
                                        icon: Icon(
                                          Icons.alarm,
                                          color: appBlack,
                                        ),
                                        label: Text(
                                          DateFormat.Hms().format(
                                              DateTime.tryParse(
                                                      article?.publishedAt ??
                                                          "") ??
                                                  DateTime.now()),
                                          style:
                                              Theme.of(context).textTheme.body2,
                                          maxLines: 1,
                                        ),
                                        onPressed: () => "",
                                      ),
                                    )
                                  ],
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
//                      onTap: () => Navigator.of(context)
//                          .pushNamed('facilities/detail/${it.id}'),
                  );
                },
              );
            },
          );
        }),
        controller: _ctrl,
      ),
    );
  }
}
