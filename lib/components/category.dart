import 'package:flutter/material.dart';
import 'package:newsapp/components/item_grid.dart';
import 'package:newsapp/styles/colors.dart';

class CategoryComponent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return NestedScrollView(
      headerSliverBuilder: (context, _) {
        return [
          SliverAppBar(
            pinned: true,
            backgroundColor: appBlack,
            title: Text("News App"),
            centerTitle: true,
          )
        ];
      },
      body: ItemGrid(gridData: [
        {"title": "Business"},
        {"title": "Entertainment"},
        {"title": "General"},
        {"title": "Health"},
        {"title": "Science"},
        {"title": "Sports"},
        {"title": "Technology"},
      ]),
    );
  }
}
