import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:newsapp/components/item_grid.dart';
import 'package:newsapp/services/http_service.dart';
import 'package:newsapp/styles/colors.dart';

class SourceComponent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final http = HttpService();

    return NestedScrollView(
      headerSliverBuilder: (context, _) {
        return [
          SliverAppBar(
            pinned: true,
            backgroundColor: appBlack,
            title: Text("News App"),
            centerTitle: true,
          )
        ];
      },
      body: FutureBuilder(
        future: http.getSources(),
        builder: (context, AsyncSnapshot<Map<dynamic, dynamic>> fut) {
          if (fut.connectionState == ConnectionState.waiting) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          if (fut.hasError) {
            return Center(
              child: Text("Ooops data could not be fetched at this time", textAlign: TextAlign.center,),
            );
          }
          final data = fut.data["sources"].map(
                (it) {
              return Map<String, dynamic>.from({'title': it["name"], "data": it});
            },
          ).toList();
          return ItemGrid(
            gridData: List<Map>.from(data),
          );
        },
      ),
    );
  }
}
