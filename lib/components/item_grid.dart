import 'package:flutter/material.dart';
import 'package:newsapp/styles/colors.dart';
import 'package:newsapp/utils/dimensions.dart';
import 'package:newsapp/utils/insets.dart';

class ItemGrid extends StatelessWidget {
  final List<Map<dynamic, dynamic>> gridData;

  ItemGrid({@required this.gridData});

  @override
  Widget build(BuildContext context) {
    final insets = NewsInsets(context);
    final sizer = NewsDimension(context);
    return GridView.builder(
      padding: insets.symmetric(
        vertical: 3,
        horizontal: 7,
      ),
      itemCount: gridData.length,
      semanticChildCount: gridData.length,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisSpacing: 10,
        mainAxisSpacing: 10,
        crossAxisCount: 2,
        childAspectRatio: 10 / 8.8,
      ),
      itemBuilder: (context, idx) {
        final grid = gridData[idx];
        return GestureDetector(
          onTap: () async {
            Navigator.of(context).pushNamed(grid["route"]);
          },
          child: Card(
            elevation: 0,
            margin: insets.zero,
            shape: RoundedRectangleBorder(
              side: BorderSide(color: appBlack, width: 2),
              borderRadius: BorderRadius.circular(sizer.setWidth(2)),
            ),
            color: appWhite,
            child: Padding(
              padding: insets.all(4),
              child: Center(
                child: Text(
                  grid["title"],
                  style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
