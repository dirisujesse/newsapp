import 'package:flutter/widgets.dart' show BuildContext, MediaQuery, MediaQueryData;

class NewsDimension {
  MediaQueryData _queryData;
  NewsDimension(BuildContext context) {
    _queryData = MediaQuery.of(context);
  }

  double get width {
    return _queryData.size.width;
  }

  double get height {
    return _queryData.size.height;
  }

  double setHeight(double percentage) {
    if (percentage == 0) {
      return 0;
    }
    return height * (percentage / 100);
  }

  double setWidth(double percentage) {
    if (percentage == 0) {
      return 0;
    }
    return width * (percentage / 100);
  }
}
