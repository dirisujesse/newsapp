import 'package:flutter/widgets.dart' show BuildContext, MediaQuery ;

class NewsFontSizer {
  num _shortestSide;
  NewsFontSizer(BuildContext context) {
    _shortestSide = MediaQuery.of(context).size.shortestSide;
  }

  num sp(double percentage) {
    return ((_shortestSide) * (percentage / 1000)).ceil().toDouble();
  }
}
