import 'package:flutter/material.dart';
import 'package:newsapp/components/category.dart';
import 'package:newsapp/components/source.dart';
import 'package:newsapp/components/tab.dart';
import 'package:newsapp/styles/colors.dart';

class HomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _HomePageState();
  }
}

class _HomePageState extends State<HomePage> {
  PageController _ctrl;
  final List<Widget> _pages = [
    HomeComponent(),
    CategoryComponent(),
    SourceComponent(),
  ];

  @override
  void initState() {
    _ctrl = PageController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView.builder(
        controller: _ctrl,
        itemBuilder: (context, idx) {
          return _pages[idx];
        },
      ),
      bottomNavigationBar: BottomNavigationBar(
        selectedItemColor: appBlack,
        unselectedItemColor: appBlack,
        onTap: (idx) {
          _ctrl.animateToPage(
            idx,
            duration: Duration(milliseconds: 200),
            curve: Curves.linear,
          );
        },
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text("Home"),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.album),
            title: Text("Categories"),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.inbox),
            title: Text("Sources"),
          ),
        ],
        showUnselectedLabels: true,
      ),
    );
  }
}
