import 'package:flutter/material.dart';

class AppTextStyle {
  static const appHeader = const TextStyle(
    fontFamily: 'OpenSans',
    fontSize: 30.0,
  );
  static const appText = const TextStyle(
    fontSize: 14.0,
  );
}